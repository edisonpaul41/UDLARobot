﻿#include <Servo.h>

Servo servoIzquierdo;
Servo servoDerecho;
Servo servoBoca;

int posIzquierdo = 95; //Derecha 75  //Izquierda 115 //medio 95
int posDerecha = 95; // Derecha 75 //Izquierda 110 //medio 95
int posBoca = 85; //Cerrada 85 //Abierta 75
void setup()
{
	Serial.begin(9600);
	Serial.println("Hello world!");
	servoIzquierdo.attach(9);
	servoDerecho.attach(3);
	servoBoca.attach(11);
	servoBoca.write(posBoca);
	servoDerecho.write(posDerecha);
	servoIzquierdo.write(posIzquierdo);
}


void loop() { // run over and over
	if (Serial.available()) {
		char message = Serial.read();
		switch (message) {
		case 'a':
			Serial.println("Moving Eyes to the right");
			moverOjosDerecha();
			break;
		case 'b':
			Serial.println("Moving eyes to the left");
			moverOjosIzquierda();
			break;
		case 'c':
			Serial.println("Hablando corto");
			hablar(2);
			break;
		case 'd':
			Serial.println("Hablando medio");
			hablar(4);
			break;
		case 'e':
			Serial.println("Hablando largo");
			hablar(6);
			break;
		case 'f':
			Serial.println("Hablando largo");
			hablar(9);
			break;
		case 'g':
			Serial.println("Centrando ojos");
			centrarOjos();
			break;
		default:
			Serial.println("No Data");
		}
	}
}
void centrarOjos() {
	servoDerecho.write(95);
	servoIzquierdo.write(95);
	delay(450);
}
void hablar(int segundos) {
	for (int a = 0; a < segundos; a++) {
		servoBoca.write(75);
		delay(250);
		servoBoca.write(85);
		delay(250);
	}
}
void moverOjosDerecha() {
	servoDerecho.write(75);
	servoIzquierdo.write(75);
	delay(450);
}
void moverOjosIzquierda() {
	servoDerecho.write(110);
	servoIzquierdo.write(110);
	delay(450);
}