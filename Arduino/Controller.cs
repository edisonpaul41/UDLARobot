﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Udla.Mosquera.Robot
{
    class ArduinoController
    {
        private SerialPort port;
        public ArduinoController()
        {
            this.CreateConnection();
        }
        public void CreateConnection()
        {
            port = new SerialPort("COM6", 9600);
            port.Open();
        }
        public void CloseConnection()
        {
            port.Close();
        }
        public void mirarIzquierda()
        {
            port.Write("b");
        }
        public void mirarDerecha()
        {
            port.Write("a");
        }
        public void mirarCentro()
        {
            port.Write("g");
        }
        public void hablar(int duration)
        {
            if(duration == 2)
            {
                port.Write("c");
            }
            if (duration == 4)
            {
                port.Write("d");
            }
            if (duration == 6)
            {
                port.Write("e");
            }
            if (duration == 8)
            {
                port.Write("f");
            }
        }
    }
}
