﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using LightBuzz.Vitruvius;
using Microsoft.Kinect;
using LightBuzz.Vitruvius.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace Udla.Mosquera.Robot
{
    public partial class KinectController : Window
    {
        public KinectSensor sensor;
        public MultiSourceFrameReader reader;
        GestureController gestureController;
        KinectViewer viewer, skeletonViewer, objectView, viewerDepth;
        private Body _currentBody;
        private MainWindow mainWindow;
        public Image croppedImage = new Image();
        public KinectController(KinectViewer viewer, KinectViewer skeletonViewer, KinectViewer objectView, KinectViewer viewerDepth, MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.viewer = viewer;
            this.skeletonViewer = skeletonViewer;
            this.viewerDepth = viewerDepth;
            this.objectView = objectView;
            this.sensor = KinectSensor.GetDefault();
            if (this.sensor != null)
            {
                this.sensor.Open();
                this.reader = this.sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Body | FrameSourceTypes.Infrared | FrameSourceTypes.BodyIndex);
                this.reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
                this.gestureController = new GestureController();
                this.gestureController.GestureRecognized += GestureController_GestureRecognized;
            }
        }

        private void GestureController_GestureRecognized(object sender, GestureEventArgs e)
        {
            Console.WriteLine(e.GestureType.ToString());
        }

        private void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var reference = e.FrameReference.AcquireFrame();

            // Color
            using (var frame = reference.ColorFrameReference.AcquireFrame())
            using (var depthFrame = reference.DepthFrameReference.AcquireFrame())
            using (var bodyIndexFrame = reference.BodyIndexFrameReference.AcquireFrame())
            {
                if (frame != null && depthFrame != null && bodyIndexFrame != null)
                {

                    try
                    {
                        var frameBitmap = frame.ToBitmap();
                        var noBackground = frame.GreenScreen(depthFrame, bodyIndexFrame);
                        viewer.Image = frameBitmap;
                        viewerDepth.Image = noBackground;
                        getFaces(noBackground);
                        getRightHand(noBackground);
                    }
                    catch (Exception) {
                    }

                }

            }

            // Body
            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    _currentBody = frame.Bodies().Closest();

                    if (_currentBody != null)
                    {
                       // viewer.DrawBody(_currentBody, 4.0, Brushes.Red, 4.0, Brushes.Green);
                        mainWindow.heightLabel.Content = (_currentBody.Height() + 0.05).ToString();
                    }
                    //if(_currentBody == null){
                    //    viewer.Image;
                    //}
                }
            }
            // Depth
            using (var frame = reference.DepthFrameReference.AcquireFrame())
            {
                if (frame != null && _currentBody != null)
                {
                   // viewerDepth.Image = frame.ToBitmap();
                }
            }
        }
        private void getFaces(BitmapSource img)
        {

            croppedImage.Width = 200;
            croppedImage.Margin = new Thickness(5);

            if (_currentBody != null)
            {
                try
                {
                     CroppedBitmap cb = new CroppedBitmap(img,
                         new Int32Rect(225 + (int)((_currentBody.Joints[JointType.Head].Position.X / _currentBody.Joints[JointType.Head].Position.Z) * 300),
                              170 - (int)((_currentBody.Joints[JointType.Head].Position.Y / _currentBody.Joints[JointType.Head].Position.Z) * 350), 80, 80));

                    croppedImage.Source = cb;
                    
                    skeletonViewer.Image = croppedImage.Source;
                }
                catch (Exception)
                {

                }

            }

        }
        private void getRightHand(BitmapSource img)
        {

            croppedImage.Width = 200;
            croppedImage.Margin = new Thickness(5);

            if (_currentBody != null)
            {
                try
                {

                    CroppedBitmap cb = new CroppedBitmap(img,
                        new Int32Rect(225 + (int)((_currentBody.Joints[JointType.HandRight].Position.X / _currentBody.Joints[JointType.HandRight].Position.Z) * 300),
                             170 - (int)((_currentBody.Joints[JointType.HandRight].Position.Y / _currentBody.Joints[JointType.HandRight].Position.Z) * 350), 100, 100));
                    croppedImage.Source = cb;
                    objectView.Image = croppedImage.Source;
                    //var pointHand = _currentBody.Joints[JointType.HandRight].Position.ToPoint(Visualization.Color);
                }
                catch (Exception)
                {

                }

            }

        }
    }
}
