﻿using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using LTTS7Lib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Media;
using System.Xml.Linq;
using System.Collections;

namespace Udla.Mosquera.Robot
{
    class Speech
    {
        private KinectSensor kinect;
        private KinectAudioStream convertStream;
        private SpeechRecognitionEngine speechEngine;
        private MainWindow mainWindow;
        public LTTS7Lib._DLTTS7 talk;
        private ArduinoController arduino;

        public Speech(KinectSensor kinect, MainWindow mainWindow, ArduinoController arduino)
        {
            this.mainWindow = mainWindow;
            this.arduino = arduino;
            this.kinect = kinect;
            var audioBeamList = kinect.AudioSource.AudioBeams;
            var audioStream = audioBeamList[0].OpenInputStream();
            convertStream = new KinectAudioStream(audioStream);
            RecognizerInfo ri = TryGetKinectRecognizer();
            if (null != ri)
            {
                Console.WriteLine(this.getRandomResponse("Saludo"));
               // this.recognitionSpans = new List<Span> { forwardSpan, backSpan, rightSpan, leftSpan };

                this.speechEngine = new SpeechRecognitionEngine(ri.Id);

                // Create a grammar from grammar definition XML file.
                using (var memoryStream = new MemoryStream(Encoding.ASCII.GetBytes(BodyBasics.Properties.Resources.SpeechGrammar)))
                {
                    var g = new Grammar(memoryStream);
                    this.speechEngine.LoadGrammar(g);
                }

                this.speechEngine.SpeechRecognized += this.SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected += this.SpeechRejected;

                // let the convertStream know speech is going active
                this.convertStream.SpeechActive = true;

                this.speechEngine.SetInputToAudioStream(
                    this.convertStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                this.speechEngine.RecognizeAsync(RecognizeMode.Multiple);
                talk = new LTTS7Lib.LTTS7();
                talk.Voice = "Carlos";
                talk.Language = "SpanishMx";
            }
        }

        private static RecognizerInfo TryGetKinectRecognizer()
        {
            IEnumerable<RecognizerInfo> recognizers;

            try
            {
                recognizers = SpeechRecognitionEngine.InstalledRecognizers();
            }
            catch (COMException)
            {
                return null;
            }

            foreach (RecognizerInfo recognizer in recognizers)
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "es-MX".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }

            return null;
        }
        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            const double ConfidenceThreshold = 0.8;
            this.mainWindow.confidenceLabel.Content = e.Result.Confidence;
            this.mainWindow.phraseLabel.Content = e.Result.Text;
            this.mainWindow.confidenceLabel.BorderBrush = Brushes.Red;
            this.mainWindow.angleLabel.Content = this.kinect.AudioSource.AudioBeams[0].BeamAngle;
            if (e.Result.Confidence >= ConfidenceThreshold && this.kinect.AudioSource.AudioBeams[0].BeamAngle < 0.3 && this.kinect.AudioSource.AudioBeams[0].BeamAngle > (-0.3))
            {
                this.mainWindow.confidenceLabel.BorderBrush = Brushes.Green;
                switch (e.Result.Semantics.Value.ToString())
                {
                    case "Saludo":
                        this.arduino.hablar(4);
                        talk.Read(this.getRandomResponse("Saludo"));
                        break;
                    case "ReconocerPersona":
                        this.mainWindow.recognizePeople();
                        break;
                    case "EntrenarPersona":
                        this.mainWindow.trainPeopleRecognition();
                        break;
                    case "EntrenarObjeto":
                        this.mainWindow.trainObjectRecognition();
                        break;
                    case "ReconocerObjeto":
                        this.mainWindow.recognizeObjects();
                        break;
                    case "Descripcion":
                        this.arduino.hablar(6);
                        talk.Read(this.getRandomResponse("QueEres"));
                        break;
                    case "DatoInteresante":
                        this.arduino.hablar(8);
                        talk.Read("Sabias que " + this.getRandomResponse("HechoInteresante"));
                        break;
                }
   
            }
        }

        private void SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            Console.WriteLine("Word rejected");
        }
        public void readText(String message)
        {
            this.talk.Read(message);
        }
        public String getRandomResponse(String type)
        {
            Random rnd = new Random();
            XElement xelement = XElement.Load(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Kinect\\ResponsesGrammar.xml");
            IEnumerable<XElement> responses = xelement.Elements();
            var results = (from response in xelement.Elements(type) select response).FirstOrDefault().Elements();
            ArrayList resultsString = new ArrayList();
            foreach (XElement xEle in results)
            {
                resultsString.Add(xEle.Value);
            }
            return resultsString[rnd.Next(results.Count())].ToString();
        }
    }
}
