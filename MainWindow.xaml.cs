﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Udla.Mosquera.Robot
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window
    {
        KinectController kinect;
        DataStorageAccess database;
        DataStorageAccess databaseObject;
        Recognizer recognizer;
        ObjectRecognizer objectRecognizer;
        Speech speech;
        ArduinoController arduino;
        public MainWindow()
        {
            InitializeComponent();
            //arduino = new ArduinoController();
            database = new DataStorageAccess(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Database\\UDLA.db");
            databaseObject = new DataStorageAccess(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Database\\UDLAObject.db");
            recognizer = new Recognizer(database);
            objectRecognizer = new ObjectRecognizer(databaseObject);
            kinect = new KinectController(viewer, skeletonViewer, objectView, viewerDepth, this);
            speech = new Speech(kinect.sensor, this, arduino);
        }

        public void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (kinect.reader != null)
            {
                kinect.reader.Dispose();
            }

            if (kinect.sensor != null)
            {
                kinect.sensor.Close();
            }
           // this.arduino.CloseConnection();
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }


        public static System.Drawing.Bitmap BitmapSourceToBitmap2(BitmapSource srs)
        {
            int width = srs.PixelWidth;
            int height = srs.PixelHeight;
            int stride = width * ((srs.Format.BitsPerPixel + 7) / 8);
            IntPtr ptr = IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(height * stride);
                srs.CopyPixels(new Int32Rect(0, 0, width, height), ptr, height * stride, stride);
                using (var btm = new System.Drawing.Bitmap(width, height, stride, System.Drawing.Imaging.PixelFormat.Format1bppIndexed, ptr))
                {

                    return new System.Drawing.Bitmap(btm);
                }
            }
            finally
            {
                if (ptr != IntPtr.Zero)
                    Marshal.FreeHGlobal(ptr);
            }
        }
        public String recognizePeople()
        {
            try
            {
                var recognition = this.database.GetUsername(recognizer.RecognizeUser(BitmapSourceToBitmap2((BitmapSource)kinect.croppedImage.Source)));
                userName.Text = recognition;
                if(recognition != "")
                {
                    this.arduino.hablar(4);
                    this.speech.readText(this.speech.getRandomResponse("PersonaReconocida") + " " + recognition);
                }
                else
                {
                    this.arduino.hablar(4);
                    this.speech.readText(this.speech.getRandomResponse("NoReconocido"));
                }
                return recognition;
            }
            catch (Exception)
            {
                return "";
            }
           
        }
        public String recognizeObjects()
        {
            try
            {
                var recognition = this.databaseObject.GetUsername(objectRecognizer.RecognizeUser(BitmapSourceToBitmap2((BitmapSource)kinect.croppedImage.Source)));
                objectName.Text = recognition;
                if(recognition != "")
                {
                    this.arduino.hablar(4);
                    this.speech.readText(this.speech.getRandomResponse("ObjetoReconocido") + " " + recognition);
                }
                return recognition;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public void trainPeopleRecognition()
        {
            if (userName.Text != "")
            {
                this.arduino.mirarIzquierda();
                this.arduino.hablar(4);
                this.speech.readText(this.speech.getRandomResponse("EntrenandoPersona"));
                recognizer.saveToDatabse(BitmapSourceToBitmap2((BitmapSource)kinect.croppedImage.Source), userName.Text);
                recognizer.TrainRecognizer();
                this.arduino.mirarDerecha();
            }
            else
            {
                this.arduino.hablar(4);
                this.speech.readText("Parece que aun no me dices quien eres?");
            }
        }
        public void trainObjectRecognition()
        {
            if (objectName.Text != "")
            {
                this.arduino.mirarIzquierda();
                objectRecognizer.saveToDatabse(BitmapSourceToBitmap2((BitmapSource)kinect.croppedImage.Source), objectName.Text);
                objectRecognizer.TrainRecognizer();
                this.arduino.mirarDerecha();
            }
            else
            {
                this.arduino.hablar(4);
                this.speech.readText("Parece que aun no me has dicho que es esto?");
            }
        }
        private void Train_button_Click(object sender, RoutedEventArgs e)
        {
            this.trainPeopleRecognition();
        }
        private void Recognize_button_Click(object sender, RoutedEventArgs e)
        {
            this.recognizePeople();
        }
        private void TrainObject_button_Click(object sender, RoutedEventArgs e)
        {
            this.trainObjectRecognition();
        }
        private void RecognizeObject_button_Click(object sender, RoutedEventArgs e)
        {
            this.recognizeObjects();
        }
    }

}
