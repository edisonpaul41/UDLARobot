﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Microsoft.SqlServer.Server;
using Emgu.CV.Face;

namespace Udla.Mosquera.Robot
{
    class ObjectRecognizer
    {
        private FaceRecognizer _faceRecognizer;
        private String _recognizerFilePath;
        DataStorageAccess _dataStoreAccess;
        public ObjectRecognizer(DataStorageAccess dataStore)
        {
            _dataStoreAccess = dataStore;
            _recognizerFilePath = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Database\\trained2";
            _faceRecognizer = new EigenFaceRecognizer(80, double.PositiveInfinity);
        }
        public void saveToDatabse(Bitmap image, String username)
        {
            var faceToSave = new Image<Gray, byte>(image);
            Byte[] file;
            var filePath = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]) + "\\Database\\" + String.Format(" {0}.bmp", username);
            faceToSave.ToBitmap().Save(filePath);
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new BinaryReader(stream))
                {
                    file = reader.ReadBytes((int)stream.Length);
                }
            }
            var result = _dataStoreAccess.SaveFace(username, file);
        }
        public bool TrainRecognizer()
        {
            var allFaces = _dataStoreAccess.CallFaces("ALL_USERS");

            if (allFaces != null)
            {
                Console.WriteLine("Training DATA");
                var faceImages = new Image<Gray, byte>[allFaces.Count];
                var faceLabels = new int[allFaces.Count];
                for (int i = 0; i < allFaces.Count; i++)
                {
                    Stream stream = new MemoryStream();
                    stream.Write(allFaces[i].Image, 0, allFaces[i].Image.Length);
                    var faceImage = new Image<Gray, byte>(new Bitmap(stream));
                    faceImages[i] = faceImage.Resize(300, 300, Inter.Nearest);
                    faceLabels[i] = allFaces[i].UserId;
                }
                _faceRecognizer.Train(faceImages, faceLabels);
                _faceRecognizer.Write(_recognizerFilePath);
            }
            return true;

        }
        public int RecognizeUser(Bitmap userImage)
        {
            var faceToSave = new Image<Gray, byte>(userImage);
            _faceRecognizer.Read(_recognizerFilePath);
            var result = _faceRecognizer.Predict(faceToSave.Resize(300, 300, Inter.Nearest));
            Console.WriteLine("la distancia es " + result.Distance);
            return result.Label;
        }
    }
}
